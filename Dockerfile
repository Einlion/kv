FROM python:3.12-slim
COPY app.py /app/app.py
COPY requirements.txt /app/requirements.txt

WORKDIR /app
RUN apt-get clean \
    && apt-get -y update

RUN apt-get -y install build-essential

RUN pip install -r requirements.txt
RUN pip install gunicorn

CMD "gunicorn" "-w" "3" "-b" ":7379" "app:app"

EXPOSE 7379