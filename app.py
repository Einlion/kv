
from flask import Flask, make_response
import sqlite3

app = Flask(__name__) 

host = './db/mappings.db'

@app.route('/get/<string:k>/', methods = ['GET']) 
def get(k): 
    with sqlite3.connect(host) as conn:
        cursor = conn.cursor()
        cursor.execute('SELECT * from mappings where key = ?', (k,))
        data = cursor.fetchone()
    if data is not None:
        response = make_response(data[1], 200)
        response.mimetype = "text/plain"
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
    else:
        response = make_response("No entry found", 200)
        response.mimetype = "text/plain"
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response

@app.route('/set/<string:k>/<string:val>', methods = ['GET']) 
def set(k, val): 
    try:
        with sqlite3.connect(host) as conn:
            cursor = conn.cursor()
            cursor.execute('REPLACE INTO mappings VALUES(?,?)', (k,val))
        response = make_response("1", 200)
        response.mimetype = "text/plain"
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
    except:
        response = make_response("0", 200)
        response.mimetype = "text/plain"
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
  

with app.app_context():
    with sqlite3.connect(host) as conn:
        cursor = conn.cursor()
        cursor.execute("CREATE TABLE IF NOT EXISTS mappings (key text, value text)")
